﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1;
using System.Data;
using System.Web.UI.WebControls;


namespace WebApplication1.Controllers
{

    public class EditPageController : Controller
    {
        // GET: EditPage

        public ActionResult Index()
        {

            string id = Request["id"];

            Models.ArticlesModel AllCodes = Functions.GetArticles(id);

            return View(AllCodes);

        }

        public ActionResult EditPrinters()
        {

            Models.PrintersModel Printers = Functions.GetPrinters();
            return View(Printers);
        }

        public ActionResult EditLayouts()
        {
            Models.LayoutsModel Layouts = Functions.GetLayouts(null, true);
            return View(Layouts);
        }

        public ActionResult EditArtecoArticles()
        {

            Models.AllModels Model = new AllModels();
            Model.ArticlesModel = Functions.GetArticles("0", true);
            Model.ArtecoArticlesModel = Functions.GetArticles("0");
            return View(Model);
        }

        public ActionResult EditLayoutArticles()
        {
            Models.LayoutsArticleModel Model = new LayoutsArticleModel();
            Model.ArticlesModel = Functions.GetArticles(null);
            Model.LayoutsModel = Functions.GetLayouts(null, true, true);
            string selectedid = Model.ArticlesModel.SelectedArticles.First();
            Model.UsedLayoutsModel = Functions.GetLayouts(selectedid, false, true);
            return View(Model);
        }

        public String GetCodeDetails(string codeID)
        {

            SqlDataReader result;
            result = Functions.doSelectQuery("SELECT * FROM " + SETTINGS.DATABASE + " WHERE CODE_INT='" + codeID + "'");
            if (result != null)
            {
                DataTable dt = new DataTable();

                dt.Load(result);

                string table = "";

                table += "<form method=\"post\" action=\"EditPage/submitform\"><table>";

                int colcount = 0;

                int maxcols = dt.Columns.Count;
                maxcols = maxcols / 15;

                foreach (DataColumn col in dt.Columns)
                {
                    if (colcount == 0)
                    {
                        table += "<tr>";

                    }

                    table += "<td style=\"padding-left: 20px;\">";
                    table += col.ToString();
                    table += "</td>";
                    table += "<td>";

                    TextBox tb = new TextBox();
                    tb.ID = col.ToString();
                    tb.Width = 300;

                    table += "<input type=\"text\" maxlength=\"255\" name=\"" + col.ToString() + "\" id=\"" + col.ToString() + "\" ";
                    if (dt.Rows.Count == 1)
                    {
                        table += "value = \"" + dt.Rows[0][dt.Columns.IndexOf(col)].ToString() + "\" ";
                    }
                    if (tb.ID == "CODE_INT")
                    {
                        table += "readonly ";
                    }
                    table += " /> ";
                    table += "</td>";

                    colcount++;

                    if (colcount > maxcols)
                    {
                        table += "</tr>";
                        colcount = 0;

                    }
                }
                if (colcount != 0)
                {
                    table += "</tr>";
                }

                //Table end.
                table += "<tr><td><input type=\"submit\" value=\"Submit\" style=\"float: right;\"></td></tr></table></form>";

                return table;
            }
            return "An error occurred :( Please Refresh!";
        }

        [HttpPost]
        public void submitform()
        {
            string codeID = Request["CODE_INT"];
            SqlDataReader result;
            result = Functions.doSelectQuery("SELECT * FROM " + SETTINGS.DATABASE + " WHERE CODE_INT='" + codeID + "'");

            //Populating a DataTable from database.
            DataTable dt = new DataTable();

            dt.Load(result);


            string queryString = "UPDATE " + SETTINGS.DATABASE + " SET ";

            if (dt.Rows.Count == 1)
            {
                string column =  "";
                string value = "";
                foreach (DataColumn col in dt.Columns)
                {
                    column = col.ToString();
                    value = Request[col.ToString()];
                    if (column != "CODE_INT" && value != "")
                    {
                        queryString += "\"" + column + "\"" + "= '" + value.Replace("'", "''") + "', ";
                    }
                }
            }
            queryString = queryString.Remove(queryString.Length - 2);
            queryString += " WHERE CODE_INT = '" + codeID + "'";

            Functions.doUpdateQuery(queryString);

            Response.Redirect("/LAB3/EditPage?id=" + codeID);
        }

        [HttpPost]
        public void PrinterPost(Models.PrintersModel printer)
        {
            string action = Request["action"];

            string printername = Request["printername"];
            string printerid = printer.SelectedPrinters.First();

            if (action == "Update" && printername != "")
            {
                string querystring = "UPDATE " + SETTINGS.DB_PRINTERS + " SET PRINTER_NAME = '" + printername + "' WHERE PRINTER_NAME = '" + printerid + "'";
                Functions.doUpdateQuery(querystring);
            }
            else if (action == "New" && printername != "")
            {
                string querystring = "INSERT INTO " + SETTINGS.DB_PRINTERS + " (PRINTER_NAME) VALUES ('" + printername + "')";
                Functions.doUpdateQuery(querystring);
            }
            else if (action == "Delete" && printername != "")
            {
                string querystring = "DELETE FROM " + SETTINGS.DB_PRINTERS + " WHERE PRINTER_NAME = '" + printerid + "'";
                Functions.doUpdateQuery(querystring);
            }

            Response.Redirect("/LAB3/EditPage/EditPrinters");

        }


        [HttpPost]
        public void LayoutPost(Models.LayoutsModel layout)
        {
            string action = Request["action"];

            string layoutname = Request["layoutname"];
            string layoutid = layout.SelectedLayouts.First();

            if (action == "Update" && layoutname != "")
            {
                string querystring = "UPDATE " + SETTINGS.DB_LABELSLIST + " SET LABEL_NAME = '" + layoutname + "' WHERE LABEL_NAME = '" + layoutid + "'";
                Functions.doUpdateQuery(querystring);
            }
            else if (action == "New" && layoutname != "")
            {
                string querystring = "INSERT INTO " + SETTINGS.DB_LABELSLIST + " (LABEL_NAME) VALUES ('" + layoutname + "')";
                Functions.doUpdateQuery(querystring);
            }
            else if (action == "Delete" && layoutname != "")
            {
                string querystring = "DELETE FROM " + SETTINGS.DB_LABELSLIST + " WHERE LABEL_NAME = '" + layoutid + "'";
                Functions.doUpdateQuery(querystring);
            }

            Response.Redirect("/LAB3/EditPage/EditLayouts");

        }


        [HttpPost]
        public void ArtecoArticlesPost(Models.AllModels Models)
        {
            string action = Request["action"];

            if (action == "Save")
            {
                string querystring = "UPDATE " + SETTINGS.DATABASE_USED + " SET ARTECO = '0'";
                Functions.doUpdateQuery(querystring);

                if (Models.ArtecoArticlesModel.SelectedArticles != null)
                {
                    querystring = "UPDATE " + SETTINGS.DATABASE_USED + " SET ARTECO = '1' WHERE ";

                    IList<string> strings = new List<string> { };

                    foreach (string id in Models.ArtecoArticlesModel.SelectedArticles)
                    {
                        strings.Add("CODE_INT = '" + id + "'");
                    }


                    querystring += string.Join(" OR ", strings);

                    Functions.doUpdateQuery(querystring);
                }
            }
            Response.Redirect("/LAB3/EditPage/EditArtecoArticles");
        }

        [HttpPost]
        public void ArtecoLayoutsPost(Models.LayoutsArticleModel Models)
        {
            string action = Request["action"];

            if (action == "Save")
            {
                string selectedarticle = Models.ArticlesModel.SelectedArticles.First();

                string querystring = "DELETE FROM " + SETTINGS.DB_LABELSLINK + " WHERE CODE_INT = '" + selectedarticle + "'";
                Functions.doUpdateQuery(querystring);

                if (Models.UsedLayoutsModel.SelectedLayouts != null)
                {
                    querystring = "INSERT INTO " + SETTINGS.DB_LABELSLINK + "(CODE_INT, LABEL_ID) VALUES ";
                  
                    IList <string> strings = new List<string> { };

                    foreach (string id in Models.UsedLayoutsModel.SelectedLayouts)
                    {
                        strings.Add("(" + selectedarticle + ", " + id + ")");
                    }
                    
                    querystring += string.Join(", ", strings);

                    Functions.doUpdateQuery(querystring);
                }
            }
            Response.Redirect("/LAB3/EditPage/EditLayoutArticles");
        }

    }
}