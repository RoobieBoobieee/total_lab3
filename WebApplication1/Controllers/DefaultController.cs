﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default

        public ActionResult Index()
        {
            

            Models.AllModels ViewModel = new Models.AllModels();            
            ViewModel.ArticlesModel = Functions.GetArticles();
            string selectedid = null;
            if (ViewModel.ArticlesModel.SelectedArticles.Count() != 0)
            {
                selectedid = ViewModel.ArticlesModel.SelectedArticles.First();
            }
            ViewModel.LayoutsModel = Functions.GetLayouts(selectedid);          
            ViewModel.PrintersModel = Functions.GetPrinters();

            return View(ViewModel);
            
        }


        [HttpPost]
        public void Print(Models.AllModels model)
        {
            IEnumerable<string> codeID = model.ArticlesModel.SelectedArticles;
            IEnumerable<string> layout = model.LayoutsModel.SelectedLayouts;
            IEnumerable<string> printer = model.PrintersModel.SelectedPrinters;
            

            SqlDataReader result;
            result = Functions.doSelectQuery("SELECT * FROM " + SETTINGS.DATABASE + " WHERE CODE_INT='" + codeID.First() + "'");

            IList<string> strings = new List<string> { };
            IList<string> strings2 = new List<string> { };
            DataTable dt = new DataTable();

            dt.Load(result);

            foreach (DataColumn col in dt.Columns)
            {
                strings.Add("\"" + col.ColumnName + "\"");
                strings2.Add("\"" + dt.Rows[0][dt.Columns.IndexOf(col)].ToString() + "\"");
            }

            strings.Add("\"batchnum\"");
            strings.Add("\"fillingdate\"");
            strings2.Add("\"" + Request["batchnum"] + "\"");

            string date = Request["fillingdate"];
            if (date != "")
            {
                DateTime fillingdate = Convert.ToDateTime(date);
                date = fillingdate.ToString("dd-MM-yy");
            }
            strings2.Add("\"" + date + "\"");

            string data = string.Join(",", strings);
            data = data.Replace(System.Environment.NewLine, "");
            string data2 = string.Join(",", strings2);
            data2 = data2.Replace(System.Environment.NewLine, "");

            string count = Request["lblqty"];

            string[] lines = {  "%BTW% /AF=\"" + SETTINGS.BTWPATH + layout.First() + ".BTW\" /PRN=\"" + printer.First() + "\" /D=%Trigger File Name% /R=3 /P/C=" + count,
                                "%END%",
                                data, data2};

        string path = SETTINGS.DDPATH;
            
            using (StreamWriter outputFile = new StreamWriter(path + Functions.printUnixTimeStamp() +".dd"))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }
            Response.Redirect("Index");
            
        }

        public ActionResult GetLayouts(string codeID, bool useids = false)
        {
            LayoutsModel Layouts;
            Layouts = Functions.GetLayouts(codeID, false, useids);
                        
            return Json(Layouts.Layouts, JsonRequestBehavior.AllowGet);
        }


        }

    }