﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class ArticlesModel
    {
        public IEnumerable<string> SelectedArticles { get; set; }
        public IEnumerable<SelectListItem> Articles { get; set; }
    }

    public class LayoutsModel
    {
        public IEnumerable<string> SelectedLayouts { get; set; }
        public IEnumerable<SelectListItem> Layouts { get; set; }
    }

    public class PrintersModel
    {
        public IEnumerable<string> SelectedPrinters { get; set; }
        public IEnumerable<SelectListItem> Printers { get; set; }
    }

    public class AllModels
    {
        public ArticlesModel ArticlesModel { get; set; }
        public ArticlesModel ArtecoArticlesModel { get; set; }
        public LayoutsModel LayoutsModel { get; set; }
        public PrintersModel PrintersModel { get; set; }
    }

    public class LayoutsArticleModel
    {
        public ArticlesModel ArticlesModel { get; set; }
        public LayoutsModel LayoutsModel { get; set; }
        public LayoutsModel UsedLayoutsModel { get; set; }
    }

    public static class SETTINGS
    {
        public static string DATABASE = "LAP_DATA_ARTICLES";
        public static string DATABASE_USED = "LAP_DATA_ARTICLES_USED_VIEW";
        public static string DB_LABELS = "LAP_DATA_ARTICLES_LABELSLINKVIEW";
        public static string DB_LABELSLINK = "LAP_DATA_ARTICLES_LABELSLINK";
        public static string DB_LABELSLIST = "LAP_DATA_ARTICLES_LABELS";
        public static string DB_PRINTERS = "LAP_DATA_PRINTERS";
        public static string DDPATH = @"C:\Labels\LAB3_";
        public static string BTWPATH = @"C:\BARTENDER\LAB3\";

    }
}